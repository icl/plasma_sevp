!>
!> @file
!>
!>  PLASMA is a software package provided by:
!>  University of Tennessee, US,
!>  University of Manchester, UK.
!>
!> @precisions normal z -> s d c
!>
!>    @brief Tests PLASMA symmetric eigenvalue decomposition

      PROGRAM TEST_ZGEADD

      USE, INTRINSIC :: ISO_FORTRAN_ENV
      USE OMP_LIB
      USE PLASMA

      IMPLICIT NONE

      integer, parameter       :: sp = REAL32
      integer, parameter       :: dp = REAL64

     ! set working precision, this value is rewritten for different precisions
      integer,     parameter   :: wp = dp
      integer,     parameter   :: m = 4000
      integer,     parameter   :: n = 2000
      complex(wp), parameter   :: zmone = -1.0

      complex(wp), allocatable :: A(:,:), B(:,:), Aref(:,:), Bref(:,:)

!      real(wp),    allocatable :: work(:)
      integer                  :: seed1(4) = [0, 0, 0, 1]
      integer                  :: seed2(4) = [1, 0, 1, 0]

      real(wp)                 :: Bnorm, error, tol, diffnorm

      integer                  :: infoPlasma
      logical                  :: success = .false.

      integer                  :: lda, ldb

     ! Testing variables
      complex(wp), allocatable :: B_recon(:,:)
      complex(wp)              :: alpha, beta
      integer                  :: transA
      integer                  :: normtype

      ! Performance variables
      real(dp) :: tstart, tstop, telapsed

      ! External functions
      real(wp), external :: dlamch, zlanhe, zlange
      

      tol = 50.0 * dlamch('E')
      print *, "tol:", tol


     ! Define transpose situation (for transposed A, set this to "PlasmaTrans"):
      transA = PlasmaNoTrans

     ! Define addition scenario:
      alpha=1.
      beta=1.

     ! Set matrix dimensions:
      if(transA == PlasmaNoTrans) then
        lda = max(1,m)
      else
        lda = max(1,n)
      end if
      ldb = max(1,m)

      ! Allocate matrices A and B
      if(transA == PlasmaNoTrans) then
        allocate(A(m,n), stat=infoPlasma)
      else
        allocate(A(n,m), stat=infoPlasma)
      end if
      allocate(B(m,n), stat=infoPlasma)
      allocate(Bref(m,n), stat=infoPlasma)


      ! Generate random matrices A and B
      if(transA == PlasmaNoTrans) then
        call zlarnv(1, seed1, lda*n, A)
      else
        call zlarnv(1, seed1, lda*m, A)
      end if
      call zlarnv(1, seed2, lda*n, B)

      if(transA == PlasmaNoTrans) then
        allocate(Aref(m,n), stat=infoPlasma)
      else
        allocate(Aref(n,m), stat=infoPlasma)
      end if
      Aref = A
      Bref = B

      !==============================================
      ! Initialize PLASMA.
      !==============================================
      call plasma_init(infoPlasma)

! For manual enforement of tile size and inner-block size, use the following !
!
!      call plasma_set(PlasmaNb, 16, infoPlasma)
!      call plasma_set(PlasmaIb, 8, infoPlasma)

      tstart = omp_get_wtime()

      !==============================================
      ! Perform Matrix Addition.
      !==============================================

      call plasma_zgeadd( transA, m, n, alpha, A, lda, beta, B, ldb, infoPlasma )

      tstop     = omp_get_wtime()
      telapsed  = tstop-tstart

      print *, "Time:", telapsed
      print *, ""

     ! Check matrix;

      print*,"Checking solution accuracy:"
      allocate(B_recon(m,n), stat=infoPlasma)

     ! Perform native matrix addition
      if(transA == PlasmaNoTrans) then
        B_recon = alpha*Aref + beta*Bref
      else
        B_recon = alpha*transpose(Aref) + beta*Bref
      end if

!     Compute One norm of B_recon: ||B_recon||_1     
      normtype = PlasmaOneNorm
      Bnorm    = 0.

      Call plasma_zlange(PlasmaInfNorm, m, n, B_recon, ldb, Bnorm)

!     Compute difference norm: ||B-B_recon||_1 / n*||B_recon||_1
      B_recon  = B_recon - B
      diffnorm = 0.

      Call plasma_zlange(normtype, m, n, B_recon, ldb, diffnorm)
      call plasma_finalize(infoPlasma)
      error = diffnorm/(m*Bnorm)

      if (error < tol)  success = .true.
      print *, "||A-Arecon||_1 / n*||A_recon||_1:", error
      print *, "success:                         ", success

      !==============================================
      ! Finalise PLASMA.
      !==============================================
      call plasma_finalize(infoPlasma)

      ! Deallocate matrix Aref
      deallocate(Aref, stat=infoPlasma)

      ! Deallocate matrix A
       deallocate(A, stat=infoPlasma)

      ! Deallocate arrays needed for error analysis, etc
       deallocate(B_recon,  stat=infoPlasma)

      END PROGRAM TEST_ZGEADD
