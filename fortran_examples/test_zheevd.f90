!>
!> @file
!>
!>  PLASMA is a software package provided by:
!>  University of Tennessee, US,
!>  University of Manchester, UK.
!>
!> @precisions normal z -> s d c
!>
!>    @brief Tests PLASMA symmetric eigenvalue decomposition

      PROGRAM TEST_ZHEEVD

      USE, INTRINSIC :: ISO_FORTRAN_ENV
      USE OMP_LIB
      USE PLASMA

      IMPLICIT NONE

      integer, parameter       :: sp = REAL32
      integer, parameter       :: dp = REAL64

     ! set working precision, this value is rewritten for different precisions
      integer,     parameter   :: wp = dp
      integer,     parameter   :: n = 2000
      complex(wp), parameter   :: zmone = -1.0

      complex(wp), allocatable :: A(:,:), Aref(:,:)
      complex(wp), allocatable :: eigvec(:,:)     
      real(wp),    allocatable :: eigval(:)

!      real(wp),    allocatable :: work(:)
      integer                  :: seed(4) = [0, 0, 0, 1]

      real(wp)                 :: Anorm, error, tol, diffnorm

      integer                  :: infoPlasma, i
      logical                  :: success = .false.

      integer                  :: eigflag = PlasmaEigValVec
      integer                  :: uploPlasma = PlasmaLower
      integer                  :: lda, ldq

      type(plasma_desc_t)      :: descT

     ! Testing variables
      complex(wp), allocatable :: A_recon(:,:), diag(:,:)
      complex(wp)              :: alpha, beta
      integer                  :: transA, transB
      integer                  :: normtype

      ! Performance variables
      real(dp) :: tstart, tstop, telapsed

      ! External functions
      real(wp), external :: dlamch, zlanhe, zlange
      

      tol = 50.0 * dlamch('E')
      print *, "tol:", tol

      lda = max(1,n)
      ldq = max(1,n)

      ! Allocate matrix A
      allocate(A(lda,n), stat=infoPlasma)

      ! Generate random Hermitian positive definite matrix A
      call zlarnv(1, seed, lda*n, A)
      A = A * conjg(transpose(A))
      do i = 1, n
        A(i,i) = A(i,i) + n
      end do

      allocate(Aref(lda,n), stat=infoPlasma)
      Aref = A

     ! allocate workspace for eigenvalues
      allocate(eigval(n), stat=infoPlasma)

     ! allocate workspace for eigenvectors
      allocate(eigvec(lda,n), stat=infoPlasma)

      !==============================================
      ! Initialize PLASMA.
      !==============================================
      call plasma_init(infoPlasma)

! For manual enforement of tile size and inner-block size, use the following !
!
!      call plasma_set(PlasmaNb, 16, infoPlasma)
!      call plasma_set(PlasmaIb, 8, infoPlasma)

      tstart = omp_get_wtime()

      !==============================================
      ! Perform Eigenvalue Decomposition.
      !==============================================

      call plasma_zheevd( eigflag, uploPlasma, n, A, lda, descT, eigval, eigvec, ldq, infoPlasma )

      tstop     = omp_get_wtime()
      telapsed  = tstop-tstart

      !==============================================
      ! Finalise PLASMA.
      !==============================================
 
      call plasma_finalize(infoPlasma)

      print *, "Time:", telapsed
      print *, ""

! Check matrix; re-form Aref as PDP^T (using PLASMA routines)

      print*,"Checking solution accuracy:"

      allocate(A_recon(lda,n), stat=infoPlasma)
      allocate(diag(n,n), stat=infoPlasma)

! Build diaginal matrix from eigenvalues:

       diag=0.
       Do i=1,n
         diag(i,i) = eigval(i)
       end do

! First step;  A_recon = P * D

       A_recon=0.
       alpha=1.
       beta=0.
       transA = PlasmaNoTrans
       transB = PlasmaNoTrans

       call plasma_init(infoPlasma)
       call plasma_zgemm(transA, transB, n, n, n, alpha, eigvec, n, diag, n, beta, A_recon, n, infoplasma)

! Second step; A_recon = A_recon * P^T
     
       alpha    = 1.
       beta     = 0.

       transA = PlasmaNoTrans
       transB = PlasmaTrans

       call plasma_zgemm(transA, transB, n, n, n, alpha, A_recon, n, eigvec, n, beta, A_recon, n, infoplasma)

!      Compute Frobenius norm of Aref: ||Aref||_1     
       normtype = PlasmaOneNorm
       Anorm    = 0.

       Call plasma_zlanhe(normtype, uploPlasma, n, Aref, n, Anorm)

!      Compute difference norm: ||A-Aref||_1 / n*||Aref||_1
       A_recon  = A_recon - Aref
       diffnorm = 0.

       Call plasma_zlange(normtype, n, n, A_recon, n, diffnorm)
       call plasma_finalize(infoPlasma)

       error = diffnorm/(n*Anorm)

       if (error < tol)  success = .true.

       print *, "||A-Aref||_1 / n*||Aref||_1:", error
       print *, "success:                    ", success


      ! Deallocate matrix Aref
       deallocate(Aref, stat=infoPlasma)

      ! Deallocate matrix A
       deallocate(A, stat=infoPlasma)

      ! Deallocate eigenvalue workspace
       deallocate(eigval, stat=infoPlasma)

      ! Deallocate eigenvector workspace
       deallocate(eigvec, stat=infoPlasma)

      ! Deallocate arrays needed for error analysis, etc
       deallocate(A_recon,  stat=infoPlasma)
       deallocate(diag,     stat=infoPlasma)


      END PROGRAM TEST_ZHEEVD
