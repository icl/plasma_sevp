/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions normal z -> c d s
 *
 **/

#include "core_blas.h"
#include "plasma_types.h"
#include "plasma_internal.h"
#include "core_lapack.h"
#include <omp.h>

/***************************************************************************//**
 *
 * @ingroup core_trd
 *
 *  Is a part of the tridiagonal reduction algorithm (bulgechasing)
 *  It correspond to a local driver of the kernels that should be executed on a
 *  single core.
 *
 *******************************************************************************
 *
 * @param[in] n
 *          The order of the matrix A. n >= 0.
 *
 * @param[in] nb
 *          The size of the Bandwidth of the matrix A,
 *          which correspond to the tile size. nb >= 0.
 *
 * @param[in,out] A
 *          plasma_complex64_t array, dimension (lda,n)
 *          On entry, the (nb+1)-by-n band lower hermetian matrix to
 *          be reduced to tridiagonal.
 *          On exit, the diagonal and first subdiagonal of A are over-
 *          written by the corresponding elements of the tridiagonal.
 *
 * @param[in] lda
 *          (input) INTEGER
 *          The leading dimension of the array A.  LDA >= max(1,nb+1).
 *
 * @param[out] V
 *          plasma_complex64_t array, dimension (n) if wantz=0
 *          or ldv*Vblksiz*blkcnt if wantz>0.
 *          The scalar elementary reflectors are written in this
 *          array.
 *
 * @param[out] TAU
 *          plasma_complex64_t array, dimension (n) if wantz=0
 *          or Vblksiz*Vblksiz*blkcnt if wantz>0.
 *          The scalar factors of the elementary reflectors are written
 *          in this array.
 *
 * @param[in] Vblksiz
 *          Local parameter to Plasma. It correspond to the local bloccking
 *          of the applyQ2 used to apply the orthogonal matrix Q2.
 *
 * @param[in] wantz
 *          integer tobe 0 or 1. if wantz=0 the V and TAU are not stored on
 *          only they are kept for next step then overwritten.
 *
 * @param[in] i
 *          Integer that refer to the current sweep. (outer loop).
 *
 * @param[in] sweepid
 *          Integer that refer to the sweep to chase.(inner loop).
 *
 * @param[in] m
 *          Integer that refer to a sweep step, to ensure order dependencies.
 *
 * @param[in] grsiz
 *          Integer that refer to the size of a group.
 *          group mean the number of kernel that should be executed sequentially
 *          on the same core.
 *          group size is a trade-off between locality (cache reuse) and parallelism.
 *          a small group size increase parallelism while a large group size increase
 *          cache reuse.
 *
 * @param[in] work
 *          Workspace of size nb. Used by the core_zhbtype[123]cb.
 *
 ******************************************************************************/
void core_ztrd( int n, int nb,
                    plasma_complex64_t *A, int lda,
                    plasma_complex64_t *V,
                    plasma_complex64_t *TAU,
                    int Vblksiz, int wantz,
                    int i, int sweepid, int m, int grsiz,
                    plasma_complex64_t *work)
{
    int k, shift=3;
    int myid, colpt, stind, edind, blklastind, stepercol;

    k = shift / grsiz;
    stepercol = (k*grsiz == shift) ? k : k+1;
    for (k = 0; k < grsiz; k++){
        myid = (i-sweepid)*(stepercol*grsiz) +(m-1)*grsiz + k+1;
        if(myid%2 ==0) {
            colpt      = (myid/2) * nb + 1 + sweepid - 1;
            stind      = colpt - nb + 1;
            edind      = imin(colpt, n);
            blklastind = colpt;
        } else {
            colpt      = ((myid+1)/2)*nb + 1 +sweepid -1 ;
            stind      = colpt-nb+1;
            edind      = imin(colpt,n);
            if( (stind>=edind-1) && (edind==n) )
                blklastind = n;
            else
                blklastind = 0;
        }
	

        	if( myid == 1 )
           	core_zhbtype1cb(n, nb, A, lda, V, TAU, stind-1, edind-1, sweepid-1, Vblksiz, wantz, work);
        	else if(myid%2 == 0)
           	core_zhbtype2cb(n, nb, A, lda, V, TAU, stind-1, edind-1, sweepid-1, Vblksiz, wantz, work);
        	else // if(myid%2 == 1)
           	core_zhbtype3cb(n, nb, A, lda, V, TAU, stind-1, edind-1, sweepid-1, Vblksiz, wantz, work);
        if(blklastind >= (n-1))  break;
    }
}

void core_omp_ztrd( int n, int nb,
                        plasma_complex64_t *A, int lda,
                        plasma_complex64_t *V,
                        plasma_complex64_t *TAU,
                        int Vblksiz, int wantz,
                        int i, int sweepid, int m, int grsiz,
                        int *PCOL, int *ACOL, int *MCOL,
                        plasma_workspace_t work,
                        plasma_sequence_t *sequence, plasma_request_t *request)
{
    
	#pragma omp task depend(in:PCOL[0])         \
        depend(in:ACOL[0])                      \
        depend(out:MCOL[0])
	{
		if (sequence->status == PlasmaSuccess) {
			int tid = omp_get_thread_num();
			core_ztrd( n, nb,
                    	A,lda,
                    	V,TAU,
                    	Vblksiz,wantz,
                    	i,sweepid,m,grsiz,
                    	(plasma_complex64_t*)work.spaces[tid]);
		}
	}
}

