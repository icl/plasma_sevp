/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions normal z -> s d c
 *
 **/

#include "plasma_async.h"
#include "plasma_context.h"
#include "plasma_descriptor.h"
#include "plasma_internal.h"
#include "plasma_types.h"
#include "plasma_workspace.h"
#include "core_blas.h"
#include <omp.h>
#include <string.h>

#undef REAL
#define COMPLEX




/***************************************************************************/
/**
 *  Parallel bulge chasing column-wise - dynamic scheduling
 **/
/***************************************************************************/
void plasma_pzhbtrd(plasma_enum_t uplo, int n, int nb, int Vblksiz,
                     plasma_complex64_t *A, int lda,
                     plasma_complex64_t *V, plasma_complex64_t *TAU,
                     double *D, double *E, int wantz,
                     plasma_workspace_t work,
                     plasma_sequence_t *sequence, plasma_request_t *request)
{

    // Check sequence status.
    if (sequence->status != PlasmaSuccess) {
        plasma_request_fail(sequence, request, PlasmaErrorSequence);
        return;
    }
    
    if ( uplo != PlasmaLower ) {
        plasma_request_fail(sequence, request, PlasmaErrorNotSupported);
        return;
    }
    
    // Quick return 
    if (n == 0) {
        return;
    }


    if ( nb == 0 ) {
        memset(E, 0, (n-1)*sizeof(double));
        #ifdef COMPLEX
            for (int i=0; i<n; i++){
                D[i] = cabs(A[lda*i]); // A is band storage
            }
        #else
            for (int i=0; i<n; i++){
                D[i] = A[lda*i];
            }
        #endif
        return;
    }

    if ( nb == 1 ) {
        
        // Case used when first stage has been applied or all real cases
        
        if( uplo == PlasmaLower ){
            for (int i=0; i < n-1; i++) {
                D[i] = creal(A[i*lda]);
                E[i] = creal(A[i*lda+1]);
            }
            D[n-1] = creal(A[(n-1)*lda]);
        }
        else {
            for (int i=0; i < n-1; i++) {
                D[i] = creal(A[i*lda+nb]);
                E[i] = creal(A[i*lda+nb-1]);
            }
            D[n-1] = creal(A[(n-1)*lda+nb]);
            E[n-1] = 0.;
        }
    }
    return;

    //=================
    // General case  //
    //================
    
    int *DEP   = (int *) calloc((size_t)(n+1), sizeof(int));
    int *MAXID = (int *) calloc((size_t)(n+1), sizeof(int));
    memset(MAXID,0,(n+1)*sizeof(int));
    
    int grsiz = 1;
    if( nb > 160 ) {
        grsiz = 2;
    }
    else if( nb > 100 ) {
        if( n < 5000 )
            grsiz = 2;
        else
            grsiz = 4;
    } else {
        grsiz = 6;
    }
    
    int  thgrsiz = n;

    int  shift=3;
    int i = shift/grsiz;
    int stepercol =  i*grsiz == shift ? i:i+1;

    i       = (n-1)/thgrsiz;
    int thgrnb  = i*thgrsiz == (n-1) ? i:i+1;

    int PCOL; // dependency on the ID of the master of the group of the previous column.
    int ACOL; // dependency on the ID of the master of the previous group of my column.
    int MCOL; // OUTPUT dependency on the ID, to be used by the next ID.
    int  thed, thgrid, grid, grnb;
    int m, stt, st, ed;
    int mylastid, myid, sweepid;
    int blklastind, stind, edind, colpt;
    #pragma omp parallel
    #pragma omp master
    {
        for (thgrid = 1; thgrid<=thgrnb; thgrid++){
            stt  = (thgrid-1)*thgrsiz+1;
            thed = imin( (stt + thgrsiz -1), (n-1));
            for (i = stt; i <= n-1; i++){
                ed=imin(i,thed);
                if(stt>ed)break;
                for (m = 1; m <=stepercol; m++){
                    st=stt;
                    for (sweepid = st; sweepid <=ed; sweepid++){
                        myid     = (i-sweepid)*(stepercol*grsiz) +(m-1)*grsiz + 1;
                        mylastid = myid+grsiz-1;
                        PCOL     = mylastid+shift-1;
                        MAXID[sweepid] = myid;
                        PCOL     = imin(PCOL,MAXID[sweepid-1]);
                        grnb     = PCOL/grsiz;
                        grid     = grnb*grsiz == PCOL ? grnb:grnb+1;
                        PCOL     = (grid-1)*grsiz +1; 
                        ACOL     = myid-grsiz;
                        if(myid==1)ACOL=0;
                        MCOL     = myid;
                        
                        core_omp_ztrd(n, nb, A, lda,
                                          V, TAU,
                                          Vblksiz, wantz,
                                          i, sweepid, m, grsiz,
                                          &DEP[PCOL], &DEP[ACOL], &DEP[MCOL], 
                                          work,
                                          sequence, request);
                        
                        if(mylastid%2 ==0){
                            blklastind      = (mylastid/2)*nb+1+sweepid-1;
                        }else{
                            colpt      = ((mylastid+1)/2)*nb + 1 +sweepid -1 ;
                            stind      = colpt-nb+1;
                            edind      = imin(colpt,n);
                            if( (stind>=edind-1) && (edind==n) )
                                blklastind=n;
                            else
                                blklastind=0;
                        }
                        if(blklastind >= (n-1))  stt=stt+1;
                    } // for sweepid=st:ed
                } // for m=1:stepercol
            } // for i=1:n-2
        } // for thgrid=1:thgrnb
    }
        
    free(DEP);
    free(MAXID);
    //================================================
    //  store resulting diag and lower diag D and E
    //  note that D and E are always real
    //================================================
    
    // sequential code here so only core 0 will work 
    if( uplo == PlasmaLower ) {
        for (i=0; i < n-1 ; i++) {
            D[i] = creal(A[i*lda]);
            E[i] = creal(A[i*lda+1]);
        }
        D[n-1] = creal(A[(n-1)*lda]);
    } else { 
        for (i=0; i<n-1; i++) {
            D[i] = creal(A[i*lda+nb]);
            E[i] = creal(A[i*lda+nb-1]);
        }
        D[n-1] = creal(A[(n-1)*lda+nb]);
    } // end PlasmaUpper 

    return;
}
