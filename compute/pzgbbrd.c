/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions normal z -> s d c
 *
 **/

#include "plasma_async.h"
#include "plasma_context.h"
#include "plasma_descriptor.h"
#include "plasma_internal.h"
#include "plasma_types.h"
#include "core_blas.h"

#include <string.h>

#undef REAL
#define COMPLEX

/*
 * The storage of the band matrix A is described below:
 * note that we keep same description as Lapack meaning that
 * a lower band start at diagonal (row=0) and below diag are
 * at row i such as
 * For Lower: A(i-j   , j) = A(i,j)
 * For upper: A(nb+i-j, j) = A(i,j)
 *       ----------------------------------
 * nb+1 |               band A             |
 *       ----------------------------------
 * For our reduction from band bidiag to bidiag
 * a bulge is created on both side of the Lower/upper
 * band and thus we allocate nb more on each side.
 *
 * ldab  = 3*nb+1;
 * AB looks like:
 *       __________________________________
 * nb   |               zero               |
 *       ----------------------------------
 * nb+1 |               band A             |
 *       ----------------------------------
 * nb   |_______________zero_______________|
 *
 * */
// For band storage 
#define AL(m_, n_) (A + nb + lda * (n_) + ((m_)-(n_)))
#define AU(m_, n_) (A + nb + lda * (n_) + ((m_)-(n_)+nb))

/**
 *  Parallel bulge chasing Reduction from BAND Bidiagonal
 *  column-wise - dynamic scheduling
 **/
/***************************************************************************/
void plasma_pzgbbrd(plasma_enum_t uplo, int minmn, int nb, int Vblksiz,
                                   plasma_complex64_t *A, int lda,
                                   plasma_complex64_t *VQ, plasma_complex64_t *TAUQ,
                                   plasma_complex64_t *VP, plasma_complex64_t *TAUP,
                                   double *D, double *E, int wantz,
                                   plasma_workspace_t work,
                                   plasma_sequence_t *sequence, plasma_request_t *request)
{
    // Return if failed sequence.
    if (sequence->status != PlasmaSuccess)
        return;

    // Quick return
    if (minmn == 0) {
        return;
    }
    
    //
    // The matrix is diagonal. We just copy it (abs for complex) and store it in D
    // sequential code here is better so only core 0 will work
    
    if ( nb == 0 ) {
        memset(E, 0, (minmn-1)*sizeof(double));
        #ifdef COMPLEX
        if( uplo == PlasmaLower ){
            //PlasmaLower 
            for (int i=0; i<minmn; i++){
                D[i]  = cabs(*AL(i,i));
            }
        }else{
            // PlasmaUpper 
            for (int i=0; i<minmn; i++){
                D[i]  = cabs(*AU(i,i));
            }
        }
        #else
        if( uplo == PlasmaLower ){
            // PlasmaLower 
            for (int i=0; i<minmn; i++){
                D[i]  = *AL(i,i);
            }
        }else{
            // PlasmaUpper 
            for (int i=0; i<minmn; i++){
                D[i]  = *AU(i,i);
                }
        }
        #endif
        return;
    }
    

    
    if ( nb == 1 ) {
        // Case used when this function is standalone and so the off diagonal
        // element require to be real. (Only for complex) 
        
        // Case used when first stage has been applied or all real cases
        if( uplo == PlasmaLower ){
            for (int i=0; i < minmn-1; i++) {
                D[i] = creal(*AL(i,i));
                E[i] = creal(*AL(i+1,i));
            }
            D[minmn-1] = creal(*AL(minmn-1,minmn-1));
        }
        else {
            for (int i=0; i < minmn-1; i++) {
                D[i] = creal(*AU(i,i));
                E[i] = creal(*AU(i,i+1));
            }
                D[minmn-1] = creal(*AU(minmn-1,minmn-1));
        }
    }
    return;

    //=============
    // General case     
    //=============
    
    int *DEP   = (int *) malloc((size_t)(minmn+1)*sizeof(int));
    int *MAXID = (int *) malloc((size_t)(minmn+1)*sizeof(int));
    memset(MAXID,0,(minmn+1)*sizeof(int));

    // Tuning the value of grsiz
    int grsiz = 1;
    if( nb > 160 ) {
        grsiz = 2;
    }
    else if( nb > 100 ) {
        if( minmn < 5000 )
            grsiz = 2;
        else
            grsiz = 4;
    } else {
        grsiz = 6;
    }
    
    int thgrsiz = minmn;

    int shift=3;
    int i = shift/grsiz;
    int stepercol =  i*grsiz == shift ? i:i+1;    
    i       = (minmn-1)/thgrsiz;
    int thgrnb  = i*thgrsiz == (minmn-1) ? i:i+1;


    int PCOL; // dependency on the ID of the master of the group of the previous column.
    int ACOL; // dependency on the ID of the master of the previous group of my column.
    int MCOL; // OUTPUT dependency on the ID, to be used by the next ID.
    int  mylastid, grnb, grid;
    int blklastind, colpt;
    int myid,  stt, st, ed, stind, edind;
    int  thgrid, thed,  sweepid, m;
    #pragma omp parallel
    #pragma omp master
    {
        
        for (thgrid = 1; thgrid<=thgrnb; thgrid++){
            stt  = (thgrid-1)*thgrsiz+1;
            thed = imin( (stt + thgrsiz -1), (minmn-1));
            for (i = stt; i <= minmn-1; i++){
                ed=imin(i,thed);
                if(stt>ed)break;
                for (m = 1; m <=stepercol; m++){
                    st=stt;
                    for (sweepid = st; sweepid <=ed; sweepid++){
                        myid     = (i-sweepid)*(stepercol*grsiz) +(m-1)*grsiz + 1;
                        mylastid = myid+grsiz-1;
                        PCOL     = mylastid+shift-1; 
                        MAXID[sweepid] = myid;
                        PCOL     = imin(PCOL,MAXID[sweepid-1]);
                                                               
                        grnb     = PCOL/grsiz;
                        grid     = grnb*grsiz == PCOL ? grnb:grnb+1;
                        PCOL     = (grid-1)*grsiz +1; 
                        ACOL     = myid-grsiz;
                        if(myid==1)ACOL=0;
                        MCOL     = myid;
                        core_omp_zbrd(
                            uplo, minmn, nb, A, lda,
                            VQ, TAUQ, VP, TAUP,
                            Vblksiz, wantz,
                            i, sweepid, m, grsiz,
                            &DEP[PCOL], &DEP[ACOL], &DEP[MCOL],
                            work, sequence, request);
                        if(mylastid%2 ==0){
                            blklastind      = (mylastid/2)*nb+1+sweepid-1;
                        }else{
                            colpt      = ((mylastid+1)/2)*nb + 1 +sweepid -1 ;
                            stind      = colpt-nb+1;
                            edind      = imin(colpt,minmn);
                            if( (stind>=edind-1) && (edind==minmn) )
                                blklastind=minmn;
                            else
                                blklastind=0;
                        }
                        if(blklastind >= (minmn-1))  stt=stt+1;
                    } // for sweepid=st:ed
                } // for m=1:stepercol
            } // for i=1:minmn-2
        } // for thgrid=1:thgrnb
        
    }
    free(DEP);
    free(MAXID);
    //==========================================================
    //  store resulting diag and lower diag D and E
    //  note that D and E are always real after the bulgechasing
    //==========================================================
    
    // sequential code here so only core 0 will work 
    memset(D, 0, minmn*sizeof(double));
    memset(E, 0, (minmn-1)*sizeof(double));
    if( uplo == PlasmaLower ){
        for (i=0; i < minmn-1; i++) {
            D[i] = creal(*AL(i,i));
            E[i] = creal(*AL(i+1,i));
        }
        D[minmn-1] = creal(*AL(minmn-1,minmn-1));
    }
    else {
        for (i=0; i < minmn-1; i++) {
            D[i] = creal(*AU(i,i));
            E[i] = creal(*AU(i,i+1));
        }
        D[minmn-1] = creal(*AU(minmn-1,minmn-1));
    }
    return;
}
#undef AL
#undef AU
